> This is a part of a Nodejs Backed integration with socket for a project I am currently working on.

## Summary of Project

The project is a designed to gamify learning expereince for African student.
The socket IO layer enables student to communicate and challenge each other in real time on diffrent subjects.

I have written this controllers and Helpers to handles services such as Auth, User Rgeistration,Image processing, real-time messaging integrations , postings etc.


## Challenges

Major challenge for me was integration of socket io for realtime communication, especially communicating UI changes from frontend to the backend.
I am solving the challenges majorly by going through documenation , googling and sometime jsut relaxing and coming back to the problem.And a lot of trial-and-errors.
I have previuosly woorked with Google's Firestore real-time database for this kind of proble, so using socket provided another unique type of challenges



## Code

Code is written in NodeJS as the whole Architecture is based on Javacsript. I am using NextJS . Decided to use React just take my knowledge beyond the basics.

## Architecture

![Architecture](Archy.jpg "Architecture")
