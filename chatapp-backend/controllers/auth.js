const Joi = require('joi');
const HttpStatus = require('http-status-codes');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// user model schema, this is a NoSQl database with Mongoose ORM
const User = require('../models/userModels');
const Helpers = require('../Helpers/helpers');
const dbConfig = require('../config/secret');

/* Handles all logic related to user auth by email and password combinations
1. Performs request body validation with Joi
2. Checks if username already exists
3. Create user if not exists or throw error
4. Validate user and login
*/
module.exports = {
  // Creates Schema with joi schema validation @params{req.body}
  async CreateUser(req, res) {
    const schema = Joi.object().keys({
      username: Joi.string()
        .min(5)
        .max(10)
        .required(),
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string()
        .min(5)
        .required(),
    });

    // error handler incase there is a validation error from joi
    const { error, value } = Joi.validate(req.body, schema);
    if (error && error.details) {
      return res.status(HttpStatus.BAD_REQUEST).json({ msg: error.details });
    }

    // checks if email exist , sends error message to user
    const userEmail = await User.findOne({
      email: Helpers.lowerCase(req.body.email),
    });
    if (userEmail) {
      return res.status(HttpStatus.CONFLICT).json({ message: 'Email already exist' });
    }
    // checks if username exist , sends error message to user
    const userName = await User.findOne({
      username: Helpers.firstUpper(req.body.username),
    });
    if (userName) {
      return res.status(HttpStatus.CONFLICT).json({ message: 'Username already exist' });
    }

    return bcrypt.hash(value.password, 10, (err, hash) => {
      if (err) {
        return res.status(HttpStatus.BAD_REQUEST).json({ message: 'Error hashing password' });
      }
      // create user if it does not already exists.
      const body = {
        username: Helpers.firstUpper(value.username),
        email: Helpers.lowerCase(value.email),
        password: hash,
      };
      User.create(body)
        .then(user => {
          const token = jwt.sign({ data: user }, dbConfig.secret, {
            expiresIn: '5h',
          });
          res.cookie('auth', token);
          res.status(HttpStatus.CREATED).json({
            message: 'User created successfully',
            user,
            token,
          });
        })
        .catch(err => {
          // catch error if user can not be created
          res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Error occured' });
        });
    });
  },
  // make checks, login User if all validation succesfull.
  async LoginUser(req, res) {
    if (!req.body.username || !req.body.password) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'No empty fields allowed' });
    }

    await User.findOne({ username: Helpers.firstUpper(req.body.username) })
      .then(user => {
        if (!user) {
          return res.status(HttpStatus.NOT_FOUND).json({ message: 'Username not found' });
        }

        return bcrypt.compare(req.body.password, user.password).then(result => {
          if (!result) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Password is incorrect' });
          }
          const token = jwt.sign({ data: user }, dbConfig.secret, {
            expiresIn: '5h',
          });
          res.cookie('auth', token);
          return res.status(HttpStatus.OK).json({ message: 'Login successful', user, token });
        });
      })
      .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'Error occured' }));
  },
};
