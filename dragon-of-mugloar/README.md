# dragons-of-mugloar

> This is a VueJS/vueX solution to BigBank's dragons of Mugloar problem. A detailed run down of the problem can  be found at [Dragons of Mugloar](https://www.dragonsofmugloar.com/)

## Build Setup

``` bash
# cd into directory
  cd dragons-of-mugu
```
## install dependencies
```npm install```

## serve with hot reload at localhost:8080
```npm run dev```

## build for production with minification
```npm run build```



For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
